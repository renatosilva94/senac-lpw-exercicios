const modeloCarro = document.getElementsByName("carro");
const imgCarro = document.getElementById("imgCarro");
const pintura = document.getElementById("ckPintura");
const alarme = document.getElementById("ckAlarme");
const valor = document.getElementById("outPreco");
const adicionaisCheckbox = [pintura, alarme];
let primeiroCarro = { modelo: "ka", preco: 45900.00, adicionais: [] };
const formataValor = { style: "currency", currency: "BRL" };

const carros = [
  { modelo: "ka", preco: 45900, adicionais: [] },
  { modelo: "fiesta", preco: 55000, adicionais: [] },
  { modelo: "focus", preco: 79000, adicionais: [] },
];

const adicionais = [
  { item: "ckPintura", preco: 1000 },
  { item: "ckAlarme", preco: 300 },
];

function carregarPrecoTotal(primeiroCarro) {
  const valorDoCarro = primeiroCarro.preco;
  const valorDosadicionais = primeiroCarro.adicionais.reduce(
    (ac, op) => (ac += op.preco),
    0
  );
  const valorTotal = valorDoCarro + valorDosadicionais;
  valor.textContent = valorTotal.toLocaleString("pt-br", formataValor);
}

window.addEventListener("load", () => {
  modeloCarro[0].checked = true;
  imgCarro.src = `img/${primeiroCarro.modelo}.png`;
  primeiroCarro.adicionais = [];
  adicionaisCheckbox.forEach((item) => {
    item.checked = false;
  });
  carregarPrecoTotal(primeiroCarro);
});

modeloCarro.forEach((item) => {
  item.addEventListener("change", () => {
    pintura.checked = false;
    alarme.checked = false;
    primeiroCarro.adicionais = [];
    primeiroCarro = carros.find((carro) => carro.modelo === item.id);
    carregarPrecoTotal(primeiroCarro);
    imgCarro.src = `img/${item.id}.png`;
  });
});

adicionaisCheckbox.forEach((item) => {
  item.addEventListener("change", async () => {
    const opcional = adicionais.find((op) => {
      return op.item === item.id;
    });
    if (item.checked) {
      primeiroCarro.adicionais.push(opcional);
    } else {
      primeiroCarro.adicionais.splice(
        primeiroCarro.adicionais.indexOf(opcional),
        1
      );
    }
    carregarPrecoTotal(primeiroCarro);
  });
});

