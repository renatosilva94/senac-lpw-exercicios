export class ApostaLista {
    constructor() {
        this.apostas = []
    }

    novaAposta(aposta) {
        this.apostas.push(aposta)
    } 

    ativarDesativar(id) {
        for (const aposta of this.apostas) {
            if (aposta.id == id) {
                aposta.ativo = !aposta.ativo
                break
            }
        }
    }

    get() {
        return this.apostas
    }

    removerInativos() {
        console.log(this.apostas)
        this.apostas = this.apostas.filter(x => x.ativo)
        console.log(this.apostas)
    }

    ordenaVencedores(peso) {

        //ORDENA OS VENCEDORES E CALCULAR A MARGEM DE ERRO CASO O MESMO NÃO ACERTE CORRETAMENTE

        return this.apostas.map(aposta => ({ ...aposta, margemErro: Math.abs(aposta.peso - peso) }))
                   .sort((aposta1, aposta2) => (aposta1.margemErro - aposta2.margemErro) || 
                   (aposta1.id - aposta2.id));
      }

 
}